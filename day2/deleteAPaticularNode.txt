============================C==============================

SinglyLinkedListNode* deleteNode(SinglyLinkedListNode* llist, int position) {


    SinglyLinkedListNode * trav = llist;
    SinglyLinkedListNode * temp = NULL;
    
    int i = 0;
    
    if(position == 0) {
        
        temp = llist;
        llist = trav->next;
        free(temp);
        return llist;
    }
    
    while(i<position-1) {
        
        i++;
        trav = trav->next;
    }
    temp = trav->next;
    trav->next = trav->next->next;
    free(temp);
    return llist;
}
==========================================================