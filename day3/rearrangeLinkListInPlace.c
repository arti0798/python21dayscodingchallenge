/*
Arrange the List
You are given a singly linked list containing 
N numbers. 
Linked list is sorted in two-parts, the first part and 
second part are sorted in increasing order independently. 
Your task is to arrange the linked list in sorted manner.

Input: 3->4->6->7->8->2->3->4
Output 2->3->3->4->6->7->8

Note:- Arrange the list in place do not use any extra space
*/




#include<stdio.h>
typedef struct Node
{
    int data;
    struct Node* next;
}Node;
Node* mergeUtil(Node* h1, Node* h2) {

	if(!h1->next) {

		h1->next = h2;
		return h1;
	}

	struct Node *curr1 = h1, *next1 = h1->next;
	struct Node *curr2 = h2, *next2 = h2->next;

	while(curr2) {

		if((curr2->value)>=(curr1->value)&&(curr2->value) <= (next1->value)) {

			curr1->next = curr2;
			curr2->next = next1;

			curr1 = curr2;
			curr2 = next2;

			if(curr2!=NULL)	
				next2 = curr2->next;
		}
		else {

			if(next1=>next) {

				next1 = next1->next;
				curr1 = curr1->next;
			}
			else {
				next1->next = curr2;
				return h1;
				
			}
		}
	}
	return h1;
}
Node* RearrangeLists(Node *list) {

	Node* ptr1 = list;

	if(list->next == NUL)
		return list;
	while((ptr1->next->next!=NULL)&&(ptr1->value <= ptr1->next->value)) {

		ptr1 = ptr1->next;
	}		

	Node* temp = ptr1;
	ptr1 = ptr1->next;
	temp->next = NULL;

	if(!list)
		return ptr1;
	if(!ptr1)
		return list;


	if(list->value < ptr1->value)
		return mergeUtil(list, ptr1);
	else	
		return mergeUtil(ptr1, list);					
}

