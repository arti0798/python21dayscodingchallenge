/*
PrepBytes organized a contest and a total of 
N participants participates in the contest. All participants are assigned a unique id from 1
to N. After the contest finished, PrepBytes wants to give the prize to the participant who got the maximum score. But he is not able to decide who is the winner, because every participant got the same score. So PrepBytes decides to first arrange all participant's ids in a circle and then eliminate all the 
Mth participant until only one participant is left. First, he eliminates the Mth
 participant from the beginning, then he again starts eliminating from 
(M+1)th
 participant. 

 Prepbyte gave you a circular singly linked list containing all unique id of the participant from 1 to N
 in increasing order and an integer M
 and asked you to find the id of a participant who is left after eliminating the N−1
 participants.

INPUT
3
5 3
1 2 3 4 5
5 5
1 2 3 4 5
5 8
1 2 3 4 5
-----

Output
4
2
1

*/






#include <bits/stdc++.h>
using namespace std;
typedef struct node {

    int value;
    struct node* next;
}node;

node* getNode(node *head,int val) {

    node *element = new node;
    element->value = val;
    element->next = NULL;
    node *temp = head;
    if(head == NULL) {

        head = element;
        head->next= head;
        return head;
    }
    while(temp->next != head)
        temp = temp->next;

    temp->next = element;
    element->next = head;
    return head;    
}

struct node * COntestWinner(struct node *head,int M,int size) {

    int res = 0;
    for(int n = 1;n<=size;n++) {

        res = (res + M-1)%n + 1;
    }
    if(size == 1)
        return head;
    for(int i = 1;i<res;i++) {

        head = head->next;
    }    
    head->next = NULL;
    return head;
}

int main() {

    int t;
    cin>>t;
    while(t--) {

        node *head = NULL, *temp;
        int size,i,val,M;

        cin>>size>>M;

        for(i = 0;i  < size;i++) {

            cin>>val;
            head = getNode(head,val);

        }

        temp = COntestWinner(head,M,size);

        if(temp != NULL)
            cout << temp->value<<endl;
    }
    return 0;
}